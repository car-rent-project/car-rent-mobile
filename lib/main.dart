import 'dart:io';

import 'package:car_rental/core/theme.dart';
import 'package:car_rental/data/keys.dart';
import 'package:car_rental/pages/home.page.dart';
import 'package:car_rental/pages/sign.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await _loadHive();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return MaterialApp(
      title: 'Car Rent',
      theme: themeData,
      builder: (ctx, w) => Directionality(textDirection: TextDirection.rtl, child: w),
      home: _buildScreen,
    );
  }

  ValueListenableBuilder get _buildScreen {
    return ValueListenableBuilder(
      valueListenable: Hive.box('values').listenable(keys: [Keys.TOKEN]),
      builder: (ctx, box, widget) {
        String token = box.get(Keys.TOKEN);
        if (token == null || token.isEmpty) return SignPage();
        return HomePage();
      },
    );
  }
}

Future<void> _loadHive() async {
  try {
    Directory directory = await getApplicationDocumentsDirectory();
    Hive.init(directory.path + "/hive");
    await Hive.openBox("values");
  } catch (err) {
    debugPrint(err.toString());
  }
}
