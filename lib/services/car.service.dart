import 'package:car_rental/config.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/http.dart';

part 'car.service.g.dart';

@RestApi(baseUrl: "${baseUrl}api/cars")
abstract class CarService {
  factory CarService(Dio dio, {String baseUrl}) = _CarService;

  @GET("/")
  Future<CarListResponse> getList({
    @Query('search') String search,
    @Query("skip") int skip,
    @Query("limit") int limit,
    @Query('type') String type,
    @Query('state') String state,
  });

  @GET("/{id}")
  Future<CarResponse> getById({@Path('id') String id});

  @POST("/")
  Future<CarResponse> create({@Body() Car car});

  @PUT("/{id}")
  Future<CarResponse> update({@Path('id') String id, @Body() Car car});

  @DELETE("/{id}")
  Future<CarResponse> delete({@Path('id') String id});
}

@JsonSerializable()
class CarResponse {
  Car car;

  CarResponse({this.car});

  factory CarResponse.fromJson(Map<String, dynamic> json) => _$CarResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CarResponseToJson(this);
}

@JsonSerializable()
class CarListResponse {
  List<Car> cars;
  int count;

  CarListResponse({this.cars, this.count});

  factory CarListResponse.fromJson(Map<String, dynamic> json) => _$CarListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CarListResponseToJson(this);
}
