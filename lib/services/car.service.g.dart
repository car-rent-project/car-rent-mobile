// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car.service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarResponse _$CarResponseFromJson(Map<String, dynamic> json) {
  return CarResponse(
    car: json['car'] == null
        ? null
        : Car.fromJson(json['car'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CarResponseToJson(CarResponse instance) =>
    <String, dynamic>{
      'car': instance.car,
    };

CarListResponse _$CarListResponseFromJson(Map<String, dynamic> json) {
  return CarListResponse(
    cars: (json['cars'] as List)
        ?.map((e) => e == null ? null : Car.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    count: json['count'] as int,
  );
}

Map<String, dynamic> _$CarListResponseToJson(CarListResponse instance) =>
    <String, dynamic>{
      'cars': instance.cars,
      'count': instance.count,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _CarService implements CarService {
  _CarService(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://car-rent.liara.run/api/cars';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<CarListResponse> getList({search, skip, limit, type, state}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'search': search,
      r'skip': skip,
      r'limit': limit,
      r'type': type,
      r'state': state
    };
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CarListResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<CarResponse> getById({id}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CarResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<CarResponse> create({car}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(car?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CarResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<CarResponse> update({id, car}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(car?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CarResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<CarResponse> delete({id}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'DELETE',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = CarResponse.fromJson(_result.data);
    return value;
  }
}
