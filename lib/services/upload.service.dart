import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

import 'package:car_rental/config.dart';

part 'upload.service.g.dart';

@RestApi(baseUrl: "${baseUrl}uploads")
abstract class UploadService {
  factory UploadService(Dio dio, {String baseUrl}) = _UploadService;

  @POST("/image")
  Future<UploadResponse> image({@Part(name: 'file') File file, @SendProgress() ProgressCallback onProgress});
}

@JsonSerializable()
class UploadResponse {
  String url;

  UploadResponse({this.url});

  factory UploadResponse.fromJson(Map<String, dynamic> json) => _$UploadResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UploadResponseToJson(this);
}
