// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StateListResponse _$StateListResponseFromJson(Map<String, dynamic> json) {
  return StateListResponse(
    states: (json['states'] as List)
        ?.map(
            (e) => e == null ? null : State.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    count: json['count'] as int,
  );
}

Map<String, dynamic> _$StateListResponseToJson(StateListResponse instance) =>
    <String, dynamic>{
      'states': instance.states,
      'count': instance.count,
    };

StateResponse _$StateResponseFromJson(Map<String, dynamic> json) {
  return StateResponse(
    state: json['state'] == null
        ? null
        : State.fromJson(json['state'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StateResponseToJson(StateResponse instance) =>
    <String, dynamic>{
      'state': instance.state,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _StateService implements StateService {
  _StateService(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://car-rent.liara.run/api/states';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<StateListResponse> getList(
      {search, skip = 0, limit, parent, type}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'search': search,
      r'skip': skip,
      r'limit': limit,
      r'parent': parent,
      r'type': type
    };
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = StateListResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<StateResponse> getById({id}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = StateResponse.fromJson(_result.data);
    return value;
  }
}
