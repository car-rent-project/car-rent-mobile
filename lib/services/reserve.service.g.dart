// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reserve.service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReserveResponse _$ReserveResponseFromJson(Map<String, dynamic> json) {
  return ReserveResponse(
    reserve: json['reserve'] == null
        ? null
        : Reserve.fromJson(json['reserve'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ReserveResponseToJson(ReserveResponse instance) =>
    <String, dynamic>{
      'reserve': instance.reserve,
    };

ReserveListResponse _$ReserveListResponseFromJson(Map<String, dynamic> json) {
  return ReserveListResponse(
    reserves: (json['reserves'] as List)
        ?.map((e) =>
            e == null ? null : Reserve.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    count: json['count'] as int,
  );
}

Map<String, dynamic> _$ReserveListResponseToJson(
        ReserveListResponse instance) =>
    <String, dynamic>{
      'reserves': instance.reserves,
      'count': instance.count,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ReserveService implements ReserveService {
  _ReserveService(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://car-rent.liara.run/api/reserves';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<ReserveListResponse> getList(
      {skip, limit, populate, userID, carID, mine}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'skip': skip,
      r'limit': limit,
      r'populate': populate,
      r'user': userID,
      r'car': carID,
      r'mine': mine
    };
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveListResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ReserveListResponse> getCustomerList({skip, limit}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'skip': skip, r'limit': limit};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/customers',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveListResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ReserveResponse> getById({id}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ReserveResponse> create({reserve}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(reserve?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ReserveResponse> update({id, reserve}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(reserve?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ReserveResponse> delete({id}) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'DELETE',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ReserveResponse.fromJson(_result.data);
    return value;
  }
}
