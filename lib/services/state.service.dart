import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:car_rental/config.dart';
import 'package:car_rental/models/state.model.dart';
import 'package:retrofit/retrofit.dart';

part 'state.service.g.dart';

@RestApi(baseUrl: "${baseUrl}api/states")
abstract class StateService {
  factory StateService(Dio dio, {String baseUrl}) = _StateService;

  @GET("/")
  Future<StateListResponse> getList(
      {@Query('search') String search,
      @Query("skip") int skip = 0,
      @Query("limit") int limit,
      @Query("parent") String parent,
      @Query('type') String type});

  @GET("/{id}")
  Future<StateResponse> getById({@Path('id') String id});
}

@JsonSerializable()
class StateListResponse {
  List<State> states;
  int count;

  StateListResponse({this.states, this.count});

  factory StateListResponse.fromJson(Map<String, dynamic> json) => _$StateListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StateListResponseToJson(this);
}

@JsonSerializable()
class StateResponse {
  State state;

  StateResponse({this.state});

  factory StateResponse.fromJson(Map<String, dynamic> json) => _$StateResponseFromJson(json);

  Map<String, dynamic> toJson() => _$StateResponseToJson(this);
}
