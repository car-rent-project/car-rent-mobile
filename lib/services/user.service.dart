import 'package:car_rental/config.dart';
import 'package:car_rental/models/user.model.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'user.service.g.dart';

@RestApi(baseUrl: "${baseUrl}api/users")
abstract class UserService {
  factory UserService(Dio dio, {String baseUrl}) = _UserService;

  @POST("/signin")
  Future<HttpResponse> signIn({@Body() SignInBody data});

  @POST("/verify")
  Future<VerifyResponse> verify({@Body() VerifyBody data});

  @GET("/profile")
  Future<UserResponse> profile({@Query('populate') String populate});

  @PUT("/profile")
  Future<UserResponse> update({@Body() User user});
}

@JsonSerializable()
class UserResponse {
  User user;

  UserResponse({this.user});

  factory UserResponse.fromJson(Map<String, dynamic> json) => _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}

@JsonSerializable()
class VerifyResponse {
  String message;
  String token;
  String id;

  VerifyResponse({this.message, this.token, this.id});

  factory VerifyResponse.fromJson(Map<String, dynamic> json) => _$VerifyResponseFromJson(json);

  Map<String, dynamic> toJson() => _$VerifyResponseToJson(this);
}

@JsonSerializable(includeIfNull: false, nullable: true)
class SignInBody {
  String phone;

  SignInBody({this.phone});

  factory SignInBody.fromJson(Map<String, dynamic> json) => _$SignInBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SignInBodyToJson(this);
}

@JsonSerializable(includeIfNull: false, nullable: true)
class VerifyBody {
  String phone;
  String key;

  VerifyBody({this.phone, this.key});

  factory VerifyBody.fromJson(Map<String, dynamic> json) => _$VerifyBodyFromJson(json);

  Map<String, dynamic> toJson() => _$VerifyBodyToJson(this);
}
