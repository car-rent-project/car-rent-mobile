import 'package:car_rental/config.dart';
import 'package:car_rental/models/reserve.model.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/http.dart';

part 'reserve.service.g.dart';

@RestApi(baseUrl: "${baseUrl}api/reserves")
abstract class ReserveService {
  factory ReserveService(Dio dio, {String baseUrl}) = _ReserveService;

  @GET("/")
  Future<ReserveListResponse> getList(
      {@Query("skip") int skip,
      @Query("limit") int limit,
      @Query('populate') String populate,
      @Query('user') String userID,
      @Query('car') String carID,
      @Query('mine') int mine});

  @GET("/customers")
  Future<ReserveListResponse> getCustomerList({@Query("skip") int skip, @Query("limit") int limit});

  @GET("/{id}")
  Future<ReserveResponse> getById({@Path('id') String id});

  @POST("/")
  Future<ReserveResponse> create({@Body() Reserve reserve});

  @PUT("/{id}")
  Future<ReserveResponse> update({@Path('id') String id, @Body() Reserve reserve});

  @DELETE("/{id}")
  Future<ReserveResponse> delete({@Path('id') String id});
}

@JsonSerializable()
class ReserveResponse {
  Reserve reserve;

  ReserveResponse({this.reserve});

  factory ReserveResponse.fromJson(Map<String, dynamic> json) => _$ReserveResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ReserveResponseToJson(this);
}

@JsonSerializable()
class ReserveListResponse {
  List<Reserve> reserves;
  int count;

  ReserveListResponse({this.reserves, this.count});

  factory ReserveListResponse.fromJson(Map<String, dynamic> json) => _$ReserveListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ReserveListResponseToJson(this);
}
