import 'package:after_layout/after_layout.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:car_rental/pages/create-car.page.dart';
import 'package:car_rental/services/car.service.dart';
import 'package:car_rental/widgets/car-row.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

CarService carService = new CarService(http);

class CarsListPage extends StatefulWidget {
  @override
  _CarsListPageState createState() => _CarsListPageState();
}

class _CarsListPageState extends State<CarsListPage> with AfterLayoutMixin {
  ScrollController _scrollController = new ScrollController();
  TextEditingController _searchController = new TextEditingController();
  FocusNode _searchBarFocus = new FocusNode();

  bool _loading = true;
  List<Car> _carList = [];

  CAR_TYPE _carType;
  String _searchValue;

  @override
  void afterFirstLayout(BuildContext context) async {
    _loadCarList();
  }

  Future<void> _loadCarList() async {
    String type;
    if (_carType != null) {
      type = carTypeToText(_carType);
    }
    try {
      setState(() {
        _carList = [];
        _loading = true;
      });
      CarListResponse response = await carService.getList(type: type, search: _searchValue);
      setState(() {
        _carList = response.cars;
      });
    } catch (e) {
      DioError err = e;
      debugPrint(err.response.toString());
    } finally {
      setState(() => _loading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      top: false,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text("خودرو ها", style: TextStyle(color: Colors.blueGrey[900])),
            actions: [
              IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () async {
                    bool refresh = await Navigator.of(context).push(MaterialPageRoute(builder: (_) => CreateCarPage()));
                    if (refresh != null && refresh) {
                      _loadCarList();
                    }
                  })
            ],
            centerTitle: true,
            backgroundColor: Colors.white,
            expandedHeight: 130,
            pinned: true,
            floating: true,
            snap: true,
            elevation: 1,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                padding: const EdgeInsets.only(top: 96, right: 16, left: 16, bottom: 4),
                child: GestureDetector(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[_buildSearchBar],
                  ),
                ),
              ),
            ),
          ),
          if (_loading)
            SliverFillRemaining(
              child: Center(
                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.blueGrey)),
              ),
            )
          else
            SliverList(delegate: SliverChildListDelegate(_carList.map((car) => CarRow(car: car)).toList()))
        ],
      ),
    ));
  }

  Widget get _buildSearchBar {
    return Row(
      children: <Widget>[
        Expanded(
          child: TextField(
              focusNode: _searchBarFocus,
              onTap: () {
                _scrollController.jumpTo(0);
              },
              controller: _searchController,
              maxLines: 1,
              textInputAction: TextInputAction.search,
              cursorColor: Colors.grey,
              style: new TextStyle(
                fontSize: 14.0,
                height: 1.0,
              ),
              decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                  hintText: "چه خودرویی می خوای ؟",
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  suffixIcon: _searchValue == null || _searchValue.length == 0
                      ? null
                      : IconButton(
                          icon: Icon(
                            Icons.clear,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            _searchBarFocus.canRequestFocus = false;
                            _searchController.clear();
                            setState(() => _searchValue = null);
                            _loadCarList();
                          },
                        )),
              onChanged: (v) => setState(() => _searchValue = v),
              onSubmitted: (v) => _loadCarList()),
        ),
        _buildSearchBarToggle(iconData: CommunityMaterialIcons.car_lifted_pickup, carType: CAR_TYPE.TRUCK),
        _buildSearchBarToggle(iconData: CommunityMaterialIcons.car_side, carType: CAR_TYPE.SEDAN),
      ],
    );
  }

  Widget _buildSearchBarToggle({CAR_TYPE carType, IconData iconData}) {
    String message = carType == CAR_TYPE.SEDAN ? 'سواری' : 'باربری';
    return GestureDetector(
      onTap: () {
        if (_carType == carType)
          setState(() => _carType = null);
        else
          setState(() => _carType = carType);
        _loadCarList();
      },
      child: Tooltip(
        message: message,
        child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            margin: EdgeInsets.only(right: 4),
            height: 54,
            width: 54,
            decoration: BoxDecoration(
              color: _carType == carType ? Colors.blueGrey[100] : Colors.white,
              border: Border.all(color: Colors.black45),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Icon(iconData, size: 32)),
      ),
    );
  }
}
