import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';

class SingleValueFormPage extends StatefulWidget {
  final String title;
  final String label;
  final String hint;
  final String initialValue;

  SingleValueFormPage({this.title, this.label, this.hint, this.initialValue});

  @override
  _SingleValueFormPageState createState() => _SingleValueFormPageState();
}

class _SingleValueFormPageState extends State<SingleValueFormPage> with AfterLayoutMixin {
  FocusNode _focusNode = FocusNode();
  String value;

  @override
  void initState() {
    value = "";
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _focusNode.requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        titleSpacing: 0,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                Navigator.of(context).pop(value);
              })
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(top: 24.0, right: 8.0, left: 8.0, bottom: 16.0),
        child: Column(
          children: <Widget>[
            TextFormField(
              focusNode: _focusNode,
              initialValue: widget.initialValue,
              decoration: InputDecoration(
                  labelText: widget.label,
                  labelStyle: TextStyle(color: Colors.black87),
                  filled: true,
                  helperText: widget.hint ?? "",
                  border: InputBorder.none),
              onChanged: (v) => setState(() => value = v),
            ),
          ],
        ),
      ),
    );
  }
}
