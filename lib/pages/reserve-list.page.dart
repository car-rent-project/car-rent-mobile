import 'package:after_layout/after_layout.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/core/jalaali-datetime.dart';
import 'package:car_rental/core/loading-dialog.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:car_rental/models/reserve.model.dart';
import 'package:car_rental/models/user.model.dart';
import 'package:car_rental/pages/car-details.page.dart';
import 'package:car_rental/services/reserve.service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

ReserveService _reserveService = new ReserveService(http);

class ReserveListPage extends StatefulWidget {
  @override
  _ReserveListPageState createState() => _ReserveListPageState();
}

class _ReserveListPageState extends State<ReserveListPage> with AfterLayoutMixin {
  int _index;

  List<Reserve> _customerReserveList;
  List<Reserve> _myReserveList;

  @override
  void initState() {
    _index = 0;

    _customerReserveList = [];
    _myReserveList = [];
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    await Future.wait([_loadCustomersReserveList(), _loadMyReserveList()]);
  }

  Future<void> _loadMyReserveList() async {
    try {
      ReserveListResponse response = await _reserveService.getList(populate: 'user car', mine: 1);
      setState(() {
        _myReserveList = response.reserves;
      });
    } catch (e) {
      DioError err = e;
      debugPrint(err.response.data);
    }
  }

  Future<void> _loadCustomersReserveList() async {
    try {
      ReserveListResponse response = await _reserveService.getCustomerList();
      setState(() {
        _customerReserveList = response.reserves;
      });
    } catch (e) {
      DioError err = e;
      debugPrint(err.response.data);
    }
  }

  Future<void> _acceptReserve(Reserve reserve) async {
    try {
      showLoadingDialog(context);
      ReserveResponse response = await _reserveService.update(id: reserve.id, reserve: Reserve(accepted: true));
      setState(() {
        _customerReserveList.firstWhere((r) => response.reserve.id == r.id).accepted = true;
      });
    } catch (e) {
      debugPrint(e);
    } finally {
      dismissLoadingDialog(context);
    }
  }

  void _openCall(User user) {
    Uri call = Uri(scheme: 'tel', path: "+98" + user.phone);
    launch(call.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      top: false,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text("رزرو ها", style: TextStyle(color: Colors.blueGrey[900])),
            centerTitle: true,
            backgroundColor: Colors.white,
            expandedHeight: 120,
            pinned: true,
            floating: true,
            snap: true,
            elevation: 1,
            flexibleSpace: FlexibleSpaceBar(
                background: Container(
              padding: const EdgeInsets.only(top: 92, right: 16, left: 16, bottom: 4),
              child: Center(child: _buildSegmentedControl()),
            )),
          ),
          if (_index == 0)
            SliverList(
              delegate: SliverChildListDelegate(_customerReserveList.map((Reserve reserve) {
                return Container(
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    ..._buildTiles(reserve: reserve),
                    ListTile(
                      dense: true,
                      title: Text("کاربر"),
                      trailing: Text(User.fromJson(reserve.user)?.name ?? "بدون نام"),
                      onTap: () => _openCall(User.fromJson(reserve.user)),
                    ),
                    ListTile(
                      dense: true,
                      title: Text("وضعیت"),
                      trailing: reserve?.accepted == true
                          ? Text("تایید شده", style: TextStyle(color: Colors.green))
                          : GestureDetector(
                              child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 3),
                                  decoration:
                                      BoxDecoration(color: Colors.grey[200], borderRadius: BorderRadius.circular(4)),
                                  child: Text("تایید", style: TextStyle(color: Colors.blue))),
                              onTap: () => _acceptReserve(reserve),
                            ),
                    ),
                    Divider()
                  ]),
                );
              }).toList()),
            ),
          if (_index == 1)
            SliverList(
              delegate: SliverChildListDelegate(_myReserveList.map((Reserve reserve) {
                return Container(
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    ..._buildTiles(reserve: reserve),
                    ListTile(
                      dense: true,
                      title: Text("وضعیت"),
                      trailing: reserve?.accepted == true
                          ? Text("تایید شده", style: TextStyle(color: Colors.green))
                          : Text("انتظار", style: TextStyle(color: Colors.deepOrange)),
                    ),
                    Divider(),
                  ]),
                );
              }).toList()),
            )
        ],
      ),
    ));
  }

  CupertinoSegmentedControl<int> _buildSegmentedControl() {
    return CupertinoSegmentedControl(
      children: {
        0: Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 6),
          child: Text("رزرو های مشتری", style: TextStyle(color: _segmentTextColor(0))),
        ),
        1: Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 6),
          child: Text("رزرو های من", style: TextStyle(color: _segmentTextColor(1))),
        ),
      },
      groupValue: _index,
      pressedColor: Colors.blueGrey,
      borderColor: Colors.blueGrey,
      selectedColor: Colors.blueGrey[700],
      onValueChanged: (index) {
        setState(() => _index = index);
      },
    );
  }

  List<ListTile> _buildTiles({Reserve reserve}) {
    return [
      ListTile(
        dense: true,
        title: Text("تاریخ ثبت"),
        trailing: Text(getJalaliDate(reserve.createdAt)),
      ),
      ListTile(
        dense: true,
        title: Text("تاریخ رزرو"),
        trailing: Text(reserve?.date != null ? getJalaliDate(reserve.date.toLocal()) : ""),
      ),
      ListTile(
        dense: true,
        title: Text("خودرو"),
        trailing: Text(Car.fromJson(reserve?.car).title),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => CarDetailsPage(car: Car.fromJson(reserve?.car))));
        },
      ),
    ];
  }

  Color _segmentTextColor(int index) {
    return _index == index ? Colors.white : Colors.black;
  }
}
