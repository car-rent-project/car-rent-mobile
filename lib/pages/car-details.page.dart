import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:car_rental/config.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/core/jalaali-datetime.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:car_rental/models/reserve.model.dart';
import 'package:car_rental/services/reserve.service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:intl/intl.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:persian_datepicker/persian_datetime.dart';

final formatter = new NumberFormat("###,###,###,###,###,###,###,###");
InputDecoration inputDecoration =
    InputDecoration(filled: true, labelStyle: TextStyle(color: Colors.black87), border: InputBorder.none);

final ReserveService _reserveService = new ReserveService(http);

class CarDetailsPage extends StatefulWidget {
  final Car car;

  const CarDetailsPage({Key key, this.car}) : super(key: key);

  @override
  _CarDetailsPageState createState() => _CarDetailsPageState();
}

class _CarDetailsPageState extends State<CarDetailsPage> {
  Reserve _reserve;

  final TextEditingController textEditingController = TextEditingController();
  PersianDatePickerWidget persianDatePicker;

  @override
  void initState() {
    _reserve = Reserve(car: widget.car.id);
    persianDatePicker = PersianDatePicker(
        controller: textEditingController,
        fontFamily: 'Samim',
        minDatetime: DateTime.now().toIso8601String(),
        showGregorianDays: false,
        onChange: (v1, v2) {
          setState(() {
            _reserve.date = DateTime.parse(PersianDateTime(jalaaliDateTime: v2).toGregorian());
          });
          Navigator.of(context).pop();
        }).init();
    super.initState();
  }

  Future<void> _createReserve() async {
    if (_reserve.date == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("ابتدا یک تاریخ انتخاب کنید ."),
        duration: Duration(milliseconds: 5000),
        action: SnackBarAction(label: "باشه", onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar()),
      ));
      return;
    }
    try {
      await _reserveService.create(reserve: _reserve);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("رزرو شما با موفقیت ثبت شد ."),
        duration: Duration(milliseconds: 5000),
        action: SnackBarAction(label: "باشه", onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar()),
      ));
    } catch (e) {
      DioError err = e;
      if (err.response.statusCode == HttpStatus.conflict) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("رزور در این تاریخ امکان پذیر نیست ."),
          duration: Duration(milliseconds: 5000),
          action: SnackBarAction(label: "باشه", onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar()),
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("مشخصات خودرو")),
      body: Container(
        child: ListView(
          children: [
            Container(
              height: 250,
              child: Swiper(
                pagination: new SwiperPagination(),
                itemCount: widget.car.images.length,
                itemBuilder: (BuildContext context, int index) {
                  return new CachedNetworkImage(imageUrl: baseUrl + widget.car.images[index], fit: BoxFit.cover);
                },
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 9, horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(widget.car.title, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                      Row(
                        children: [
                          Chip(
                            label: Text("نوع : ${widget.car.type == CAR_TYPE.SEDAN ? 'سواری' : 'باربری'}",
                                style: TextStyle(color: Colors.black)),
                          ),
                          Container(width: 6),
                          Chip(
                            label: Text(getJalaliDate(widget.car.createdAt), style: TextStyle(color: Colors.black)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: 4, bottom: 16),
                      child: Text(widget.car.description, style: TextStyle(color: Colors.black87))),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              controller: textEditingController,
                              decoration: inputDecoration.copyWith(
                                  hintText: "تاریخ رزور", prefixIcon: Icon(Icons.calendar_today_sharp)),
                              onTap: () {
                                FocusScope.of(context).requestFocus(new FocusNode());
                                showModalBottomSheet(
                                  context: context,
                                  builder: (BuildContext context) => persianDatePicker,
                                );
                              },
                            ),
                          ),
                          Container(width: 6),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              alignment: Alignment.center,
                              height: 49,
                              color: Colors.grey[300].withAlpha(100),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(Icons.money, color: Colors.grey[600], size: 28),
                                  Container(width: 16),
                                  Text("${formatter.format(widget.car.price)} ریال",
                                      style: TextStyle(
                                          color: Colors.grey[600], fontSize: 16, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 16),
                        alignment: Alignment.center,
                        child: TextButton(
                          child: Text("رزرو", style: TextStyle(color: Colors.white)),
                          style: TextButton.styleFrom(backgroundColor: Colors.blueGrey),
                          onPressed: _createReserve,
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
