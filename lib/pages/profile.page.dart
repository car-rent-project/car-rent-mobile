import 'package:after_layout/after_layout.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/core/loading-dialog.dart';
import 'package:car_rental/data/keys.dart';
import 'package:car_rental/models/state.model.dart' as SM;
import 'package:car_rental/models/user.model.dart';
import 'package:car_rental/pages/singleValueForm.page.dart';
import 'package:car_rental/services/user.service.dart';
import 'package:car_rental/widgets/state-select.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

UserService _userService = UserService(http);

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with AfterLayoutMixin {
  User _user;

  @override
  void initState() {
    _user = User();
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    try {
      UserResponse response = await _userService.profile(populate: 'state');
      setState(() => _user = response.user);
    } catch (e) {
      DioError err = e;
      debugPrint(err.response.data);
    }
  }

  Future<void> _updateProfile(User user) async {
    try {
      showLoadingDialog(context);
      await _userService.update(user: user);
      await _loadProfile();
    } catch (e) {
      DioError err = e;
      debugPrint(err.response.data);
    } finally {
      dismissLoadingDialog(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("کاربری")),
      body: Container(
          child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.person),
            title: Text("نام"),
            trailing: Text(
              _user?.name ?? "",
              style: TextStyle(color: Colors.black87),
            ),
            onTap: () async {
              String str = await Navigator.of(context).push(MaterialPageRoute(
                  builder: (ctx) => SingleValueFormPage(
                        title: "ویرایش نام",
                        label: "نام",
                        hint: "نام خود را ویرایش کنید و تیک را بزنید .",
                        initialValue: _user?.name ?? "",
                      )));
              if (str != null && str.isNotEmpty) {
                _updateProfile(User(name: str));
              }
            },
          ),
          ListTile(
            leading: Icon(Icons.phone),
            title: Text("شماره تلفن"),
            trailing: Text(
              _user?.phone ?? "",
              textDirection: TextDirection.ltr,
              style: TextStyle(color: Colors.grey),
            ),
          ),
          ListTile(
            leading: Icon(Icons.location_city),
            title: Text("شهر"),
            trailing: Text(
              _user?.state != null ? SM.State.fromJson(_user.state).title : "",
              style: TextStyle(color: Colors.black87),
            ),
            onTap: () async {
              SM.State state = await showModalBottomSheet<SM.State>(
                context: context,
                builder: (BuildContext context) => StateSelect(),
              );
              if (state != null) _updateProfile(User(state: state.id));
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text("خروج"),
            onTap: () {
              Hive.box('values').delete(Keys.TOKEN);
            },
          )
        ],
      )),
    );
  }
}
