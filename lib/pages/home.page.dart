import 'package:car_rental/pages/cars-list.page.dart';
import 'package:car_rental/pages/profile.page.dart';
import 'package:car_rental/pages/reserve-list.page.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex;

  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        selectedItemColor: Colors.blueGrey[800],
        unselectedItemColor: Colors.blueGrey[300],
        onTap: (i) => setState(() => _currentIndex = i),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.directions_car_rounded), label: 'خودرو ها'),
          BottomNavigationBarItem(icon: Icon(CommunityMaterialIcons.ticket), label: 'رزرو ها'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'کاربری')
        ],
      ),
      body: LayoutBuilder(
        builder: (_, c) {
          switch (_currentIndex) {
            case 0:
              return CarsListPage();
            case 1:
              return ReserveListPage();
            case 2:
              return ProfilePage();
            default:
              return Container();
          }
        },
      ),
    );
  }
}
