import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/core/loading-dialog.dart';
import 'package:car_rental/data/keys.dart';
import 'package:car_rental/services/user.service.dart';
import 'package:car_rental/widgets/count-down-time.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

enum STATE { PHONE, CODE }

class SignPage extends StatefulWidget {
  final userService = new UserService(http);

  @override
  _SignPageState createState() => _SignPageState();
}

class _SignPageState extends State<SignPage> with AfterLayoutMixin {
  GlobalKey<FormState> _phoneForm = GlobalKey();

  FocusNode _phoneFocusNode = FocusNode();
  FocusNode _codeFocusNode = FocusNode();

  Country country = CountryPickerUtils.getCountryByIsoCode("IR");
  STATE state;
  String phone;

  bool counterEnd;

  @override
  void initState() {
    state = STATE.PHONE;
    phone = "";
    counterEnd = false;
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _phoneFocusNode.requestFocus();
  }

  _showSnackbar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: Duration(seconds: 5),
    ));
  }

  _submitPhone() async {
    bool isValid = _phoneForm.currentState.validate();
    if (isValid) {
      _phoneForm.currentState.save();
      try {
        showLoadingDialog(context);
        await widget.userService.signIn(data: SignInBody(phone: phone));
        setState(() => state = STATE.CODE);
        Future.delayed(Duration(milliseconds: 500), () {
          _codeFocusNode.requestFocus();
        });
      } catch (e) {
        DioError err = e;
        if (err.response == null) _showSnackbar("ارتباط برقرار نشد");
      } finally {
        dismissLoadingDialog(context);
      }
    }
  }

  _submitKey(v) async {
    try {
      showLoadingDialog(context);
      var data = await widget.userService.verify(data: VerifyBody(phone: phone, key: v));

      Box box = Hive.box("values");

      box.put(Keys.TOKEN, data.token);
      box.put(Keys.USER_ID, data.id);

      dismissLoadingDialog(context);
    } catch (e) {
      dismissLoadingDialog(context);
      DioError err = e;
      if (err.response == null)
        _showSnackbar("ارتباط برقرار نشد");
      else
        _showSnackbar("کد وارد شده اشتباه است");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("ورود")),
      body: Container(
        padding: const EdgeInsets.all(8.0),
        child: statedWidget(ctx: context),
      ),
    );
  }

  Widget statedWidget({BuildContext ctx}) {
    switch (state) {
      case STATE.PHONE:
        return buildPhoneForm(ctx: ctx);
      case STATE.CODE:
        return buildCodeForm(ctx: ctx);
      default:
        return Container();
    }
  }

  Form buildPhoneForm({BuildContext ctx}) {
    return Form(
      key: _phoneForm,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 12, bottom: 16),
            child: Text("شماره تلفن خود را وارد کنید."),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: TextFormField(
                  focusNode: _phoneFocusNode,
                  decoration: InputDecoration(
                    labelText: "شماره تلفن",
                    labelStyle: TextStyle(color: Colors.black87),
                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
                  ),
                  keyboardType: TextInputType.phone,
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.left,
                  onSaved: (v) {
                    if (v.startsWith("0")) v = v.substring(1);
                    debugPrint(v);
                    setState(() => phone = v);
                  },
                  validator: (v) {
                    if (v.length < 10 || v.length > 11)
                      return "شماره تلفن اشتباه وارد شده";
                    else
                      return null;
                  },
                ),
              ),
              Directionality(
                textDirection: TextDirection.ltr,
                child: Container(
                    padding: EdgeInsets.symmetric(vertical: 21, horizontal: 9),
                    margin: EdgeInsets.only(right: 6),
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey), borderRadius: BorderRadius.circular(4)),
                    child: Row(
                      children: <Widget>[
                        CountryPickerUtils.getDefaultFlagImage(country),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("+" + country.phoneCode),
                        )
                      ],
                    )),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
              child: Text(
                "بعدی",
                style: TextStyle(color: Colors.white),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              onPressed: _submitPhone,
            ),
          )
        ],
      ),
    );
  }

  Form buildCodeForm({BuildContext ctx}) {
    double width = MediaQuery.of(ctx).size.width;

    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 12),
            child: Text("کد دریافتی را وارد کنید"),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 10 / 100, vertical: 48),
            child: Directionality(
              textDirection: TextDirection.ltr,
              child: PinCodeTextField(
                autoDisposeControllers: false,
                appContext: ctx,
                focusNode: _codeFocusNode,
                length: 6,
                animationType: AnimationType.slide,
                backgroundColor: Colors.grey[50],
                cursorColor: Colors.transparent,
                onCompleted: _submitKey,
                onChanged: (v) {},
                pinTheme: PinTheme(
                  activeColor: Colors.green,
                  inactiveColor: Colors.grey,
                  shape: PinCodeFieldShape.box,
                  borderRadius: BorderRadius.all(Radius.circular(3)),
                ),
                keyboardType: TextInputType.number,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Column(
              children: <Widget>[
                CountDownTimer(
                  secondsRemaining: 120,
                  whenTimeExpires: () {
                    setState(() {
                      counterEnd = true;
                    });
                  },
                ),
                TextButton(
                  onPressed: counterEnd ? _submitPhone : null,
                  child: Text("ارسال مجدد"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
