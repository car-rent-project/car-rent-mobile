import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:car_rental/config.dart';
import 'package:car_rental/core/currency-formatter.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/core/loading-dialog.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:car_rental/models/state.model.dart' as SM;
import 'package:car_rental/services/car.service.dart';
import 'package:car_rental/services/upload.service.dart';
import 'package:car_rental/widgets/state-select.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

InputDecoration inputDecoration =
    InputDecoration(filled: true, labelStyle: TextStyle(color: Colors.black87), border: InputBorder.none);

UploadService uploadService = new UploadService(http);
CarService carService = new CarService(http);

class CreateCarPage extends StatefulWidget {
  @override
  _CreateCarPageState createState() => _CreateCarPageState();
}

class _CreateCarPageState extends State<CreateCarPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController _stateController = TextEditingController(text: '');

  Car _car;
  double _uploadValue;

  @override
  void initState() {
    _car = new Car(type: CAR_TYPE.SEDAN, images: []);
    _uploadValue = null;
    super.initState();
  }

  void _onTypeChange(CAR_TYPE type) {
    setState(() {
      _car.type = type;
    });
  }

  void _uploadImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 50);

    if (pickedFile != null) {
      File file = File(pickedFile.path);
      UploadResponse response = await uploadService.image(
          file: file,
          onProgress: (total, done) {
            setState(() {
              _uploadValue = done / total;
            });
          });
      debugPrint(response.url);
      setState(() {
        _car.images.add(response.url);
        _uploadValue = null;
      });
    }
  }

  void _createCar() async {
    if (!_formKey.currentState.validate()) return;

    try {
      showLoadingDialog(context);

      await carService.create(car: _car);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("خودرو با موفقیت ثبت شد ."),
        duration: Duration(seconds: 5),
      ));

      Navigator.of(context).pop(true);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("ثبت خودرو با مشکل مواجه شد ."),
        duration: Duration(seconds: 5),
      ));
    } finally {
      dismissLoadingDialog(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("ثبت خودرو")),
      body: Container(
        child: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.symmetric(vertical: 9, horizontal: 16),
            children: [
              SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 9),
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ..._car.images.map((image) => _buildImageView(image)).toList(),
                    if (_uploadValue == null) _buildImageUpload(),
                    if (_uploadValue != null) _buildUploading(),
                  ],
                ),
              ),
              TextFormField(
                controller: _stateController,
                decoration: inputDecoration.copyWith(labelText: "شهر"),
                onTap: () async {
                  FocusScope.of(context).requestFocus(new FocusNode());

                  SM.State state = await showModalBottomSheet<SM.State>(
                    context: context,
                    builder: (BuildContext context) => StateSelect(),
                  );
                  if (state != null) {
                    setState(() {
                      _car.state = state.id;
                      _stateController.text = state.title;
                    });
                  }
                },
                validator: (v) {
                  if (v.isEmpty)
                    return "شهر ضروری است.";
                  else
                    return null;
                },
              ),
              Container(height: 9),
              TextFormField(
                decoration: inputDecoration.copyWith(labelText: "عنوان خودرو"),
                textInputAction: TextInputAction.next,
                onChanged: (v) => setState(() => _car.title = v),
                validator: (v) {
                  if (v.isEmpty)
                    return "عنوان ضروری است.";
                  else
                    return null;
                },
              ),
              Container(height: 9),
              TextFormField(
                decoration: inputDecoration.copyWith(labelText: "توضیحات"),
                minLines: 1,
                maxLines: 3,
                onChanged: (v) => setState(() => _car.description = v),
                validator: (v) {
                  if (v.isEmpty)
                    return "توضیحات ضروری است.";
                  else if (v.length < 30)
                    return "طول توضیحات ${v.length} کارکتر است. حداقل 30 کاراکتر باشد.";
                  else
                    return null;
                },
              ),
              Container(height: 9),
              TextFormField(
                decoration: inputDecoration.copyWith(labelText: "قیمت", suffix: Text("ریال")),
                inputFormatters: [CurrencyInputFormatter()],
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                onChanged: (v) => setState(() => _car.price = int.parse(v.replaceAll(',', ''))),
                validator: (v) {
                  if (v.isEmpty)
                    return "قیمت ضروری است.";
                  else if (int.parse(v.replaceAll(',', '')) < 100000)
                    return "قیمت حداقل 100,000 ریال می باشد.";
                  else
                    return null;
                },
              ),
              Container(
                color: Colors.grey[300].withOpacity(.5),
                margin: EdgeInsets.only(top: 9),
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                child: Row(
                  children: [
                    Text("نوع : ", style: TextStyle(fontSize: 16)),
                    Radio(
                      activeColor: Colors.blueGrey,
                      value: CAR_TYPE.SEDAN,
                      groupValue: _car.type,
                      onChanged: _onTypeChange,
                    ),
                    Text("سواری"),
                    Radio(
                      activeColor: Colors.blueGrey,
                      value: CAR_TYPE.TRUCK,
                      groupValue: _car.type,
                      onChanged: _onTypeChange,
                    ),
                    Text("باربری"),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                alignment: Alignment.centerRight,
                child: FlatButton(
                  child: Text(
                    "ثبت",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blueGrey[500],
                  onPressed: _createCar,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImageView(String image) {
    return Dismissible(
      key: Key(image),
      direction: DismissDirection.up,
      onDismissed: (direction) {
        setState(() {
          _car.images.remove(image);
        });
      },
      child: Container(
          margin: EdgeInsets.only(left: 6),
          alignment: Alignment.center,
          width: 96,
          height: 96,
          color: Colors.grey[300].withOpacity(.5),
          child: Card(
            child: CachedNetworkImage(
              width: 96,
              height: 96,
              imageUrl: baseUrl + image,
              fit: BoxFit.cover,
            ),
          )),
    );
  }

  Widget _buildUploading() {
    return Container(
      alignment: Alignment.center,
      width: 96,
      height: 96,
      color: Colors.grey[300].withOpacity(.5),
      child: Container(
        width: 48,
        height: 48,
        child: CircularProgressIndicator(
          value: _uploadValue,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
        ),
      ),
    );
  }

  Widget _buildImageUpload() {
    return GestureDetector(
      onTap: _uploadImage,
      child: Container(
        width: 96,
        height: 96,
        color: Colors.grey[300].withOpacity(.5),
        child: Icon(Icons.add_photo_alternate, size: 56, color: Colors.black54),
      ),
    );
  }
}
