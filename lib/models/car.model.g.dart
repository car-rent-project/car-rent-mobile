// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Car _$CarFromJson(Map<String, dynamic> json) {
  return Car(
    id: json['_id'] as String,
    visible: json['visible'] as bool,
    state: json['state'],
    user: json['user'],
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
    title: json['title'] as String,
    description: json['description'] as String,
    price: json['price'] as int,
    type: _$enumDecodeNullable(_$CAR_TYPEEnumMap, json['type']),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$CarToJson(Car instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('visible', instance.visible);
  writeNotNull('state', instance.state);
  writeNotNull('user', instance.user);
  writeNotNull('images', instance.images);
  writeNotNull('title', instance.title);
  writeNotNull('description', instance.description);
  writeNotNull('price', instance.price);
  writeNotNull('type', _$CAR_TYPEEnumMap[instance.type]);
  writeNotNull('createdAt', instance.createdAt?.toIso8601String());
  writeNotNull('updatedAt', instance.updatedAt?.toIso8601String());
  return val;
}

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$CAR_TYPEEnumMap = {
  CAR_TYPE.SEDAN: 'sedan',
  CAR_TYPE.TRUCK: 'truck',
};
