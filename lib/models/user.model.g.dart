// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['_id'] as String,
    name: json['name'] as String,
    address: json['address'] as String,
    avatar: json['avatar'] as String,
    phone: json['phone'] as String,
    status: _$enumDecodeNullable(_$USER_STATUSEnumMap, json['status']),
    role: _$enumDecodeNullable(_$USER_ROLEEnumMap, json['role']),
    state: json['state'],
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$UserToJson(User instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('address', instance.address);
  writeNotNull('avatar', instance.avatar);
  writeNotNull('phone', instance.phone);
  writeNotNull('status', _$USER_STATUSEnumMap[instance.status]);
  writeNotNull('role', _$USER_ROLEEnumMap[instance.role]);
  writeNotNull('state', instance.state);
  writeNotNull('createdAt', instance.createdAt?.toIso8601String());
  writeNotNull('updatedAt', instance.updatedAt?.toIso8601String());
  return val;
}

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$USER_STATUSEnumMap = {
  USER_STATUS.ACTIVE: 'active',
  USER_STATUS.INACTIVE: 'inactive',
  USER_STATUS.BLOCK: 'block',
};

const _$USER_ROLEEnumMap = {
  USER_ROLE.ADMIN: 'admin',
  USER_ROLE.USER: 'user',
};
