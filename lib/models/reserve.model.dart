import 'package:json_annotation/json_annotation.dart';

part 'reserve.model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class Reserve {
  @JsonKey(name: "_id")
  String id;
  dynamic car;
  dynamic user;
  DateTime date;
  bool accepted;
  DateTime createdAt;
  DateTime updatedAt;

  Reserve({
    this.id,
    this.car,
    this.user,
    this.accepted,
    this.createdAt,
    this.updatedAt,
  });

  factory Reserve.fromJson(Map<String, dynamic> json) => _$ReserveFromJson(json);
  Map<String, dynamic> toJson() => _$ReserveToJson(this);
}
