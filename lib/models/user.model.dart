import 'package:json_annotation/json_annotation.dart';

part 'user.model.g.dart';

enum USER_STATUS {
  @JsonValue("active")
  ACTIVE,
  @JsonValue("inactive")
  INACTIVE,
  @JsonValue('block')
  BLOCK
}

enum USER_ROLE {
  @JsonValue("admin")
  ADMIN,
  @JsonValue("user")
  USER
}

enum USER_GENDER {
  @JsonValue("male")
  MALE,
  @JsonValue("female")
  FEMALE
}

@JsonSerializable(nullable: true, includeIfNull: false)
class User {
  @JsonKey(name: "_id")
  String id;
  String name;
  String address;
  String avatar;
  String phone;
  USER_STATUS status;
  USER_ROLE role;
  dynamic state;
  DateTime createdAt;
  DateTime updatedAt;

  User({
    this.id,
    this.name,
    this.address,
    this.avatar,
    this.phone,
    this.status,
    this.role,
    this.state,
    this.createdAt,
    this.updatedAt,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
