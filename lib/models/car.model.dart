import 'package:json_annotation/json_annotation.dart';

part 'car.model.g.dart';

enum CAR_TYPE {
  @JsonValue('sedan')
  SEDAN,
  @JsonValue('truck')
  TRUCK
}

@JsonSerializable(nullable: true, includeIfNull: false)
class Car {
  @JsonKey(name: "_id")
  String id;
  bool visible;
  dynamic state;
  dynamic user;
  List<String> images;
  String title;
  String description;
  int price;
  CAR_TYPE type;
  DateTime createdAt;
  DateTime updatedAt;

  Car({
    this.id,
    this.visible,
    this.state,
    this.user,
    this.images,
    this.title,
    this.description,
    this.price,
    this.type,
    this.createdAt,
    this.updatedAt,
  });

  factory Car.fromJson(Map<String, dynamic> json) => _$CarFromJson(json);
  Map<String, dynamic> toJson() => _$CarToJson(this);
}

String carTypeToText(CAR_TYPE carType) => _$CAR_TYPEEnumMap[carType];
