// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reserve.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reserve _$ReserveFromJson(Map<String, dynamic> json) {
  return Reserve(
    id: json['_id'] as String,
    car: json['car'],
    user: json['user'],
    accepted: json['accepted'] as bool,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  )..date =
      json['date'] == null ? null : DateTime.parse(json['date'] as String);
}

Map<String, dynamic> _$ReserveToJson(Reserve instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('car', instance.car);
  writeNotNull('user', instance.user);
  writeNotNull('date', instance.date?.toIso8601String());
  writeNotNull('accepted', instance.accepted);
  writeNotNull('createdAt', instance.createdAt?.toIso8601String());
  writeNotNull('updatedAt', instance.updatedAt?.toIso8601String());
  return val;
}
