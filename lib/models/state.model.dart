import 'package:json_annotation/json_annotation.dart';

part 'state.model.g.dart';

enum STATE_TYPE {
  @JsonValue("province")
  PROVINCE,
  @JsonValue("city")
  CITY
}

@JsonSerializable(nullable: true, includeIfNull: false)
class State {
  @JsonKey(name: "_id")
  String id;
  dynamic parent;
  String title;
  STATE_TYPE type;

  State({
    this.id,
    this.parent,
    this.title,
    this.type,
  });

  factory State.fromJson(Map<String, dynamic> json) => _$StateFromJson(json);

  Map<String, dynamic> toJson() => _$StateToJson(this);
}

String stateTypeToText(STATE_TYPE type) => _$STATE_TYPEEnumMap[type];
