import 'package:after_layout/after_layout.dart';
import 'package:car_rental/core/http.dart';
import 'package:car_rental/models/state.model.dart' as SM;
import 'package:car_rental/services/state.service.dart';
import 'package:flutter/material.dart';

StateService _stateService = StateService(http);

class StateSelect extends StatefulWidget {
  @override
  _StateSelectState createState() => _StateSelectState();
}

class _StateSelectState extends State<StateSelect> with AfterLayoutMixin {
  SM.STATE_TYPE _showing;
  bool _loading;
  List<SM.State> _provinceList = [];
  List<SM.State> _cityList = [];

  @override
  void initState() {
    _loading = true;
    _showing = SM.STATE_TYPE.PROVINCE;
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    _loadProvinces();
  }

  Future<void> _loadProvinces() async {
    try {
      setState(() => _loading = true);
      StateListResponse response = await _stateService.getList(type: SM.stateTypeToText(SM.STATE_TYPE.PROVINCE));
      setState(() => _provinceList = response.states);
    } finally {
      setState(() => _loading = false);
    }
  }

  Future<void> _loadCities({@required String parentID}) async {
    try {
      setState(() => _loading = true);
      StateListResponse response =
          await _stateService.getList(type: SM.stateTypeToText(SM.STATE_TYPE.CITY), parent: parentID);
      setState(() => _cityList = response.states);
    } finally {
      setState(() => _loading = false);
    }
  }

  Future<void> _selectProvince(SM.State province) async {
    await _loadCities(parentID: province.id);
    setState(() => _showing = SM.STATE_TYPE.CITY);
  }

  Future<void> _selectCity(SM.State city) async {
    Navigator.of(context).pop(city);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: double.infinity,
          color: Colors.grey[200],
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
          child: Text(_showing == SM.STATE_TYPE.PROVINCE ? "انتخاب استان" : "انتخاب شهر",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        ),
        Expanded(child: _buildBody()),
      ],
    );
  }

  Widget _buildBody() {
    if (_loading) {
      return Center(child: CircularProgressIndicator());
    } else {
      if (_showing == SM.STATE_TYPE.PROVINCE) {
        return ListView.builder(
          itemCount: _provinceList.length,
          itemBuilder: (_, i) => ListTile(
            title: Text(_provinceList[i].title),
            onTap: () => _selectProvince(_provinceList[i]),
          ),
        );
      } else {
        return ListView.builder(
          itemCount: _cityList.length,
          itemBuilder: (_, i) => ListTile(
            title: Text(_cityList[i].title),
            onTap: () => _selectCity(_cityList[i]),
          ),
        );
      }
    }
  }
}
