import 'package:cached_network_image/cached_network_image.dart';
import 'package:car_rental/config.dart';
import 'package:car_rental/core/jalaali-datetime.dart';
import 'package:car_rental/models/car.model.dart';
import 'package:car_rental/pages/car-details.page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final formatter = new NumberFormat("###,###,###,###,###,###,###,###");

class CarRow extends StatelessWidget {
  const CarRow({Key key, @required this.car}) : super(key: key);

  final Car car;

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    double size = media.size.width / 3;
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => CarDetailsPage(car: car)));
      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(right: 16, left: 16, top: 8, bottom: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: CachedNetworkImage(
                      imageUrl: baseUrl + car.images[0],
                      width: size,
                      height: size,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: size,
                      padding: EdgeInsets.all(9),
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(car.title, style: Theme.of(context).textTheme.headline6),
                                Text("نوع : ${car.type == CAR_TYPE.SEDAN ? 'سواری' : 'باربری'}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(fontWeight: FontWeight.w100, color: Colors.black54)),
                                Text("${formatter.format(car.price)} ریال", style: TextStyle(color: Colors.black54))
                              ],
                            ),
                          ),
                          Align(
                              alignment: Alignment.bottomLeft,
                              child: Text.rich(TextSpan(children: [
                                WidgetSpan(
                                    child: Padding(
                                  padding: const EdgeInsets.only(left: 6.0),
                                  child: Icon(
                                    Icons.calendar_today,
                                    size: 16,
                                    color: Colors.black87,
                                  ),
                                )),
                                TextSpan(
                                    text: getJalaliDate(car.createdAt),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(fontWeight: FontWeight.w100, color: Colors.black87)),
                              ], style: TextStyle(height: .6))))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Divider(thickness: .7)
          ],
        ),
      ),
    );
  }
}
