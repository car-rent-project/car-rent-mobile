class Keys {
  static const String TOKEN = "TOKEN";
  static const String USER_ID = "USERID";
}

class CacheKeys {
  static const String PROFILE = "PROFILE";
}