import 'package:flutter/material.dart';

ThemeData get themeData => ThemeData(
      platform: TargetPlatform.iOS,
      fontFamily: 'Samim',
      primaryColor: Colors.white,
      accentColor: Colors.blueGrey,
      snackBarTheme: SnackBarThemeData(contentTextStyle: TextStyle(fontFamily: "Samim")),
      appBarTheme: AppBarTheme(color: Colors.transparent, elevation: 0),
      iconTheme: IconThemeData(color: Colors.black),
      visualDensity: VisualDensity.adaptivePlatformDensity,
    );
