
import 'package:car_rental/config.dart';
import 'package:car_rental/data/keys.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:hive/hive.dart';

Dio get http {
  Map<String, dynamic> _removeNulls(Map<String, dynamic> input) => input..removeWhere((k, v) => v == null);
  DioCacheManager dioCacheManager = DioCacheManager(CacheConfig(baseUrl: baseUrl));
  Dio myDio = new Dio();
  Box box = Hive.box("values");

  myDio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
    String token = box.get(Keys.TOKEN);
    if (token != null) {
      options.headers.addAll({"authorization": "Bearer "+token});
    }

    if (options.queryParameters.length > 0) options.queryParameters = _removeNulls(options.queryParameters);

    return options; //continue
  }, onResponse: (Response response) async {
    // Do something with response data
    return response; // continue
  }, onError: (DioError e) async {
    // Do something with response error
    return e; //continue
  }));

  myDio.interceptors.add(dioCacheManager.interceptor);

  return myDio;
}