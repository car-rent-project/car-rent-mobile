import 'package:shamsi_date/shamsi_date.dart';

String getJalaliDate(DateTime date) {
  JalaliFormatter formatter = Jalali.fromDateTime(date).formatter;
  return "${formatter.dd} ${formatter.mN} ${formatter.yyyy}";
}

String getJalaliTime(String date) {
  int hour = DateTime.parse(date).toLocal().hour;
  int minute = DateTime.parse(date).toLocal().minute;

  return "${minute < 10 ? "0" + minute.toString() : minute} : $hour";
}
