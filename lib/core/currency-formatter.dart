import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:intl/intl.dart';

class CurrencyInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    int value = int.parse(newValue.text.replaceAll(',', ''));

    final formatter = new NumberFormat("###,###,###,###,###,###,###,###");

    String newText = formatter.format(value);

    return newValue.copyWith(text: newText, selection: new TextSelection.collapsed(offset: newText.length));
  }
}
